<?php
# подключаем ядро битрикса
use Bitrix\Main\Loader;

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

# подключаем модуль инфоблоков

Loader::includeModule('iblock');

define('IBLOCK_SOFT', 5);
$arSelect = ['ID', 'NAME', 'DATE_ACTIVE_FROM'];
$arFilter = ['IBLOCK_ID' => IBLOCK_SOFT, 'ACTIVE_DATE' => 'Y', 'ACTIVE' => 'Y'];
$res      = CIBlockElement::GetList([], $arFilter, false, ['nPageSize' => 50], $arSelect);

echo '<pre>';
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
}
echo '</pre>';
