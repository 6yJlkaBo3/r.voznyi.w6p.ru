<?php
/**
 * @global CMain                 $APPLICATION
 * @var array                    $arParams
 * @var array                    $arResult
 * @var CBitrixComponent         $component
 * @var CBitrixComponentTemplate $this
 * @var string                   $templateName
 * @var string                   $componentPath
 */

require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/header.php');
$APPLICATION->SetTitle('Linux Soft');
$APPLICATION->IncludeComponent(
    'custom:linux-soft',
    '.default',
    [
        "COMPONENT_TEMPLATE" => '.default',
    ],
    false
);
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');