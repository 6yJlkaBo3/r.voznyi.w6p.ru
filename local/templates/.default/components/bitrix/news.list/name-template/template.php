<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @global CMain                 $APPLICATION
 * @var array                    $arParams
 * @var array                    $arResult
 * @var CBitrixComponent         $component
 * @var CBitrixComponentTemplate $this
 * @var string                   $templateName
 * @var string                   $componentPath
 */
$this->setFrameMode(true);

$car         = new \Voznyi\Car\Car('lada');
$car->window = 'garrilaglass';

$car->rollStarter();

?>

<div class="new">
    <?php foreach ($arResult['ITEMS'] as $item) { ?>
        <div class="new-item">
            <h2><?=$item['NAME']?></h2>
            <p><?=$item['PREVIEW_TEXT']?></p>
            <?php if ($item['PROPERTIES']['AVAILABLE_ON_OS']['NAME']) : ?>
                <p><?=$item['PROPERTIES']['AVAILABLE_ON_OS']['NAME']?></p>
            <?php endif; ?>
        </div>
    <?php } ?>
</div>


