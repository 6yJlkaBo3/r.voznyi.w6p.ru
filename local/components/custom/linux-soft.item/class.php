<?php

use Bitrix\Iblock\Component\Tools;
use Bitrix\Iblock\Elements\ElementLinuxSoftTable;
use Bitrix\Main\Loader;

/**
 * Class LinuxSoftItem
 */
class LinuxSoftItem extends \CBitrixComponent
{
    
    /**
     * Function calls __includeComponent in order to execute the component.
     *
     * @return mixed
     *
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        
        $this->arParams['ITEM_CODE'];
        // echo '<pre>';
        // print_r($arResult);
        // echo '</pre>';
        
        $this->arResult = $this->getData();
        
        if (empty($this->arResult)) {
            $this->show404();
        }
        
        $this->arResult['LOGO'] = $this->getImage();
        
        $this->setTitle($this->arResult['NAME']);
        
        $this->includeComponentTemplate();
    }
    
    protected function show404(): void
    {
        Tools::process404(false, true, true, true);
    }
    
    /**
     * @param $itemCode
     * @return bool
     */
    protected function softExist($itemCode): bool
    {
        return in_array($itemCode, self::ITEM_CODES);
    }
    
    private function getData(): array
    {
        $item = ElementLinuxSoftTable::query()
            ->where('CODE', $this->arParams['ITEM_CODE'])
            ->addSelect('NAME')
            ->addSelect('ID')
            ->addSelect('IBLOCK_ID')
            ->addSelect('CODE')
            ->addSelect('PREVIEW_TEXT')
            ->addSelect('LOGO', 'LOGO_')
            ->fetch();
        
        return $item ?: [];
    }
    
    private function getImage(): array
    {
        $img = CFile::ResizeImageGet(
            $this->arResult['LOGO_VALUE'],
            ['width' => 300, 'height' => 300],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        
        return $img ?: [];
    }
    
    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        global $APPLICATION;
        $APPLICATION->SetTitle($title);
    }
}
