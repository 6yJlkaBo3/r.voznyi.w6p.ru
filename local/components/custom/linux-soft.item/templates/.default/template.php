<?php
/**
 * @global CMain                 $APPLICATION
 * @var array                    $arParams
 * @var array                    $arResult
 * @var CBitrixComponent         $component
 * @var CBitrixComponentTemplate $this
 * @var string                   $templateName
 * @var string                   $componentPath
 */

// echo '<pre>';
// var_dump($arResult['LOGO']);
// echo '</pre>';

?>


<div class="image">
    <img src="<?=$arResult['LOGO']['src'];?>">
</div>
<div class="items">
    <p><?=$arResult['PREVIEW_TEXT'];?></p>
</div>
