<?php

/**
 * Class LinuxSoft
 */
class LinuxSoft extends \CBitrixComponent
{
    public const DEFAULT_PAGE = 'list';
    public const SEF_FOLDER   = '/linux-soft/';
    
    public $arDefaultUrlTemplates404 = [
        'list' => 'index.php',
        'item' => '#ITEM_CODE#/',
    ];
    
    public $arResult = [
        'SEF_VARIABLES' => [],
    ];
    
    /**
     * Function calls __includeComponent in order to execute the component.
     *
     * @return mixed
     *
     */
    public function executeComponent()
    {
        $page = $this->parsePage();
        $this->includeComponentTemplate($page);
    }
    
    /**
     * @return false|string
     */
    public function parsePage()
    {
        $engine        = new CComponentEngine($this);
        $componentPage = $engine->guessComponentPath(
            self::SEF_FOLDER,
            $this->arDefaultUrlTemplates404,
            $this->arResult['SEF_VARIABLES']
        );
        
        if (strlen($componentPage) <= 0) {
            $componentPage = self::DEFAULT_PAGE;
        }
        
        return $componentPage;
    }
}
