<?php
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 */

global $APPLICATION;
$APPLICATION->IncludeComponent('custom:linux-soft.item', '', [
    'ITEM_CODE' => $arResult['SEF_VARIABLES']['ITEM_CODE']
]);
