<?php if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}
/**
 * @var array $arResult
 */

$this->setFrameMode(true);

?>

<div class="items">
    <?php foreach ($arResult['ITEMS'] as $item) { ?>
        <div class="item">
            <a href="<?=$item['CODE']?>/">
                <div class="item-name"><?=$item['NAME']?></div>
            </a>
            <div class="item-content">
                <div class="image">
                    <img src="<?=$item['LOGO']?>" alt="<?=$item['NAME']?>">
                </div>
                <div class="item-text">
                    <p><?=$item['USER_NAME']?></p>
<!--                    <p>--><?//=$item['SUM']?><!--</p>-->
                    <p><?=$item['QUANTITY']?></p>
                    <p><?=$item['PREVIEW_TEXT']?></p>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
