<?php
// if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
//     die();
// }
namespace Car;
use Bitrix\Iblock\Elements\ElementLinuxSoftTable;
use Bitrix\Main\Loader;
use Bitrix\Main\ORM\Fields\ExpressionField;

/**
 * Class LinuxSoftList
 * @method static getSqlGetFilename()
 */
class LinuxSoftList extends CBitrixComponent
{
    /**
     * @return mixed|void|null
     * @throws \Bitrix\Main\LoaderException
     */
    public function executeComponent()
    {
        Loader::includeModule('iblock');
        $this->getListOrm();
        $this->arResult['ITEMS'] = $this->getItems();
        $this->setTitle('Список софта');
        
        $this->includeComponentTemplate();
    }
    
    public function getItems(): array
    {
        $entity = ElementLinuxSoftTable::getEntity();
        
        // $entity->addField(new ExpressionField('SUM', '%s+%s', ['SUM_ONE.VALUE']));
        $entity->addField(new ExpressionField('SUM', 'SUM(PROPERTY_14)'));
//вывести запрос sql
        $sums = $entity->getDataClass()::query()
            ->addSelect('IBLOCK_SECTION_ID')
            ->addSelect(new ExpressionField('SUM', 'SUM(%s)', ['QUANTITY.VALUE']))
            ->addGroup('IBLOCK_SECTION_ID')
            ->fetchAll();
        
        
        $items = $entity->getDataClass()::query()
            ->where('ACTIVE', 'Y')
            ->addSelect('NAME')
            ->addSelect('ID')
            ->addSelect('IBLOCK_ID')
            ->addSelect('CODE')
            ->addSelect('IBLOCK_SECTION_ID')
            // ->addSelect('SUM')
            ->addSelect('QUANTITY.VALUE')
            // ->addSelect('SUM_ONE.VALUE')
            // ->addSelect('SUM_TWO.VALUE')
            ->addSelect('PREVIEW_TEXT')
            ->addSelect('CREATED_BY_USER.NAME')
            ->addSelect('CREATED_BY_USER.LAST_NAME')
            ->addSelect('LOGO.FILE', 'LOGO')
            // ->registerRuntimeField(new ExpressionField('SUM', 'CONCAT(%s,%s)', ['SUM_ONE.VALUE', 'SUM_TWO.VALUE']))
            // ->getQuery()
            ->fetchCollection();
        // \Bitrix\Iblock\ElementTable::query()->addSelect('NAME')->getQuery();
        foreach ($items as $item) {
            $file = $item->getLogo()->getFile();
            $itemSum = [];
            foreach ($sums as $sum) {
                if ($item->getIblockSectionId() === (int)$sum['IBLOCK_SECTION_ID']) {
                    $itemSum[] = $sum;
                }
            }
            
            
            $elements[] = [
                'USER_NAME'    => $item->getCreatedByUser()->getName() . ' ' . $item->getCreatedByUser()->getLastName(),
                'ID'           => $item->getId(),
                'NAME'         => $item->getName(),
                'QUANTITY'     => $item->getQuantity()->getValue(),
                // 'SUM_ONE'      => $item->getSumOne()->getValue(),
                // 'SUM_TWO'      => $item->getSumTwo()->getValue(),
                'SUM'          => $item->get('SUM'),
                'CODE'         => $item->getCode(),
                'PREVIEW_TEXT' => $item->getPreviewText(),
                'LOGO'         => '/upload/' . $file->getSubdir() . '/' . $file->getFileName(),
            ];
        }
        return $elements;
    }
    
    public function getImage(): array
    {
        $img = CFile::ResizeImageGet(
            $elements['LOGO'],
            ['width' => 300, 'height' => 300],
            BX_RESIZE_IMAGE_PROPORTIONAL,
            true
        );
        
        return $img ?: [];
    }
    
    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        global $APPLICATION;
        $APPLICATION->SetTitle($title);
    }
    
    private function getListOrm(): void
    {
    }
}

