<?php

namespace Voznyi\Main;

/**
 * Class MainNameMod
 * @package Voznyi\Main
 */
class MainNameMod
{
    
    /**
     * @param $arFields
     */
    public function onBeforeIBlockElementUpdateHandler(&$arFields): void
    {
        if (((int)$arFields['IBLOCK_ID'] === 5) && substr($arFields['NAME'], -1, 1) !== '!') {
            $arFields['NAME'] .= '!';
        }
    }
}
