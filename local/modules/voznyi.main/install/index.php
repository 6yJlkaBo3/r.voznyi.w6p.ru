<?php

use Bitrix\Main\EventManager;
use Bitrix\Main\ModuleManager;
use Voznyi\Main\MainNameMod;

/**
 * Class Voznyi_main
 */
class Voznyi_main extends CModule
{
    public $MODULE_ID;
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $MODULE_GROUP_RIGHTS;
    public $PARTNER_NAME;
    public $PARTNER_URI;
    
    /**
     * Voznyi_main constructor.
     */
    protected function __construct()
    {
        $arModuleVersion = [];
        include(dirname(__FILE__) . '/version.php');
        
        $this->MODULE_NAME         = 'Мой модуль!';
        $this->MODULE_DESCRIPTION  = 'Собраны все необходимые функции';
        $this->MODULE_ID           = 'voznyi.main';
        $this->MODULE_VERSION      = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        $this->MODULE_GROUP_RIGHTS = 'N';
        $this->PARTNER_NAME        = 'voznyi';
        $this->PARTNER_URI         = 'http://webpractik.ru';
    }
    
    public function DoInstall(): bool
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        // установочка ивента
        $eventManager = EventManager::getInstance();
        
        $eventManager->registerEventHandler(
            'iblock',
            'OnBeforeIBlockElementUpdate',
            $this->MODULE_ID,
            MainNameMod::class,
            'onBeforeIBlockElementUpdateHandler'
        );
        ModuleManager::registerModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('Установка модуля MainNameMod', $DOCUMENT_ROOT . '/local/modules/main/install/step.php');
        return true;
    }
    
    public function DoUninstall(): bool
    {
        global $DOCUMENT_ROOT, $APPLICATION;
        $eventManager = EventManager::getInstance();
        $eventManager->unRegisterEventHandler(
            'iblock',
            'OnBeforeIBlockElementUpdate',
            $this->MODULE_ID,
            MainNameMod::class,
            'OnBeforeIBlockElementUpdateHandler'
        );
        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile('Деинсталляция модуля MainNameMod', $DOCUMENT_ROOT . '/local/modules/main/install/unstep.php');
        return true;
    }
}
