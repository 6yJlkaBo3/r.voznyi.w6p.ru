<?php

CModule::IncludeModule('MainNameMod');
global $DBType;

$arClasses = [
    'MainNameMod' => 'lib/mainnamemod.php',
];

CModule::AddAutoloadClasses('MainNameMod', $arClasses);
